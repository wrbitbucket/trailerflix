//
//  Trailer.swift
//  TrailerFlix
//
//  Created by Wagner Rodrigues on 05/02/2018.
//  Copyright © 2018 Wagner Rodrigues. All rights reserved.
//

import Foundation

struct Trailer : Codable{
    let title: String
    let url: String
    let rating: Int
    let year: Int
    let poster: String
}
